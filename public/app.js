(function(){
  'use strict';

  // Declare app level module which depends on filters, and services
  var app = angular.module('app', [
    'ui.bootstrap',
    'restangular',
    'ngSanitize',
    'angular-loading-bar'
  ]);

  app.config([
    'RestangularProvider',
    function(RestangularProvider) {
      'use strict';

      // setup Restangular
      RestangularProvider.setBaseUrl('/api/');
      RestangularProvider.setResponseExtractor(function(response) {
        var newResponse = response;
        if (angular.isArray(response)) {
          angular.forEach(newResponse, function(value, key) {
            newResponse[key].originalElement = angular.copy(value);
          });
        } else {
          newResponse.originalElement = angular.copy(response);
        }

        return newResponse;
      });
      RestangularProvider.setRestangularFields({
        id: '_id'
      });
    }
  ]);

  app.controller('SearchCtrl', [
    '$scope', 'Restangular',
    function($scope, Restangular) {
      $scope.search = {
        gender: 'male',
        age: 20,
        startTime: 10,
        toTime: 12,
        day: 2,
        specialities: [],
        level: 'beginner'
      };

      $scope.ages = [];
      for(var i = 10; i < 90; i++){
        $scope.ages.push({ value: i, text: i });
      }

      $scope.keywords = '';

      $scope.users = [];

      $scope.popularSpecilities = ["Japanese", "Business", "TOEIC", "TOEFL", "Experienced"];
      $scope.selection = ["Japanese"];

      $scope.toggleSelection = function(specility) {
        var idx = $scope.selection.indexOf(specility);

        // is currently selected
        if (idx > -1) {
          $scope.selection.splice(idx, 1);
        }

        // is newly selected
        else {
          $scope.selection.push(specility);
        }
      };

      //time
      $scope.startTime = [];
      $scope.endTime = [];
      for(var i = 6; i < 18; i++){
        $scope.startTime.push(i);
        $scope.endTime.push(i);
      }

      /**
       * search
       * @returns {undefined}
       */
      $scope.searchKeywords = function(){
        var hints = [];
        return Restangular.all('specilities').customGET('search', {keywords: $scope.keywords}).then(function(specilities){
          angular.forEach(specilities, function(speciality){
            hints.push(speciality.name);
          });

          return hints;
        });
      };

      $scope.submit = function(){
        //create keywords
        angular.forEach($scope.selection, function(specility){
          $scope.search.specialities.push(specility);
        });
        if($scope.keywords !== ''){
          $scope.search.specialities.push($scope.keywords);
        }

        if($scope.search.specialities.length){
          Restangular.all('users').customPOST($scope.search, 'search').then(function(users){
            $scope.users = users;

            //reset
            $scope.search.specialities = [];
          });
        }else{
          alert('please select at least a specialty!');
        }
      };
    }
  ]);

})();