I. Install

You have to install npm (https://www.npmjs.org/) and bower (http://bower.io/) first 

1. unzip search.7z to another folder (ex: /search)
2. cd /search
3. npm install && bower install
4. open config.json to edit port and db connect url
5. node migration.js (to install sample data)
6. node index.js

II. Result
Open browser and run http://localhost:3030 (default url and port)