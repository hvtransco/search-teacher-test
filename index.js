var nconf = require('nconf');
var express = require('express');
var http = require('http');
var path = require('path');
var mongoose = require('mongoose');

var bodyParser = require('body-parser');

nconf.argv().env().file({ file: './config.json' });

//establish db connection
mongoose.connect(nconf.get("database:url"));

var app = express();

app.set('port', nconf.get('port') || process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.static(__dirname + '/public'));

require('./modules/specility')(app, nconf);
require('./modules/user')(app, nconf);

var server = app.listen(app.get('port'), function() {
  console.log('API listening on port ' + server.address().port);
});
