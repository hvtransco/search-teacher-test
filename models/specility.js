var mongoose = require("mongoose");

var SpecilitySchema = new mongoose.Schema({
  name: {type: String},
  isPopular: {type: Boolean, default: false}
});

module.exports = mongoose.model('Specility', SpecilitySchema);