var mongoose = require("mongoose");

var UserSchema = new mongoose.Schema({
  gender: {type: String, enum: ['male', 'female']},
  firstName: {type: String},
  lastName: {type: String},
  displayName: {type: String},
  email: {type: String},
  dob: {type: Date},
  phone: {type: String},
  cell: {type: String},
  picture: {type: String},
  subject: {type: String},
  specialities: [
    {
      name: {type: String},
      level: {type: String, enum: ['beginner', 'advanced']}
    }
  ],
  teachingHours: [
    {
      speciality: {type: String},
      startTime: {type: Number},
      toTime: {type: Number},
      dayInWeek: {type: Number}
    }
  ]
});

module.exports = mongoose.model('User', UserSchema);