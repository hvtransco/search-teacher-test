var User = require('../models/user');
var _ = require('lodash');

module.exports = function(app, nconf) {
  app.post('/api/users/search', function(req, res) {
    //extract data to search
    var search = req.body;

    var query = {gender: search.gender};

    //age
    var now = new Date();
    var searchDate = new Date((now.getFullYear() - req.body.age) + "-01-01");

    query.dob = {$lt: searchDate};

    //specility
    //create regex array
    var specilities = [];
    _.each(search.specialities, function(specility){
      //specilities.push(new RegExp('^' + specility, "i"));
      specilities.push({
        specialities: {
          $elemMatch: {
            name: specility,
            level: search.level
          }
        }
      });
    });

    query.$and = specilities;

    //teaching hours
    if(!!search.day){
      query.teachingHours = {
        $elemMatch: {
          startTime: {
            $gte: search.startTime
          },
          toTime: {
            $lte: search.toTime
          },
          dayInWeek: search.day
        }
      };
    }

    //console.log(specilities);
    User.find(query, function(err, users){
      if(err){
        return res.json(500, {message: 'Unknow error!'});
      }

      return res.json(users);
    });
  });
};