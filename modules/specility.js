var Specility = require('../models/specility');

module.exports = function(app, nconf) {
  /**
   * search specility by keywords
   */
  app.get('/api/specilities/search', function(req, res) {
    if(!!req.query.keywords){
      Specility.find({name: new RegExp('^' + req.query.keywords, "i")}, function(err, specilities){
        if(err){
          return res.json(500, {message: 'Unknow error!'});
        }else{
          return res.json(specilities);
        }
      });
    }else{
      return res.json(400, {message: 'invalid params'});
    }
  });
};