var nconf = require('nconf');
var path = require('path');
var async = require('async');
var request = require('request');
var mongoose = require('mongoose');

nconf.argv().env().file({file: './config.json'});

//establish db connection
mongoose.connect(nconf.get("database:url"));

var User = require('./models/user');
var Specility = require('./models/specility');

var specialties = [
  "Japanese",
  "Business",
  "TOEIC",
  "TOEFL",
  "Experienced",
  "Economics",
  "Corporate Finance",
  "Financial Modeling",
  "Financial Analysis",
  "Bloomberg",
  "Valuation",
  "Investment Banking",
  "Bloomberg Terminal",
  "Derivatives",
  "Finance",
  "Microsoft Excel",
  "Stata",
  "Equities",
  "Capital Markets",
  "Econometrics",
  "Equity Research",
  "Investments",
  "Financial Markets",
  "Mentoring",
  "Electronics",
  "Assembly",
  "Automotive",
  "Simulation",
  "Engineering",
  "PCB design",
  "ISO",
  "Electrical Engineering",
  "Simulations",
  "ASIC",
  "Embedded Systems",
  "Continuous Improvement",
  "RF",
  "Integration",
  "Analysis",
  "Software Documentation",
  "Manufacturing",
  "Manufacturing Engineering",
  "C",
  "SPC",
  "Schematic Capture",
  "5S",
  "Hardware Architecture",
  "Hardware",
  "Test Equipment",
  "Embedded Software",
  "Kaizen",
  "Test Engineering",
  "PPAP",
  "Automotive Electronics",
  "Debugging",
  "Semiconductors",
  "Failure Analysis"
];

var level = ['beginner', 'advanced'];

function getRandomSpecialties(count) {
  var shuffled = specialties.slice(0), i = specialties.length, min = i - count, temp, index;
  while (i-- > min) {
    index = Math.floor((i + 1) * Math.random());
    temp = shuffled[index];
    shuffled[index] = shuffled[i];
    shuffled[i] = temp;
  }
  return shuffled.slice(min);
}

//store specialities to db
for(var i = 0; i < specialties.length; i++){
  var specility = new Specility({
    name: specialties[i]
  });
  specility.save();
}


//create array with 20 length
//new Aray(50) does not work with async
var arr = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
async.each(arr, function(temp, cb) {
  request('http://api.randomuser.me/?results=20', function(error, response, body) {
    if (!error && response.statusCode === 200) {
      var users = JSON.parse(body).results;

      async.eachLimit(users, 10, function(userTemp, callback) {
        var specialitiesUser = [];
        var user = userTemp.user;

        //get random specialities
        var specialtiesRandom = getRandomSpecialties(Math.floor(Math.random() * specialties.length));
        //get random level
        for (var i = 0; i < specialtiesRandom.length; i++) {
          specialitiesUser.push({
            name: specialtiesRandom[i],
            level: level[Math.floor(Math.random() * level.length)]
          });
        }

        //init teachingHours
        var randomIntFromInterval = function(min,max){
            return Math.floor(Math.random()*(max-min+1)+min);
        };

        var teachingHours = [];
        for(var i = 0; i < specialitiesUser.length; i++){
          var randomStartTime = randomIntFromInterval(6, 17);
          var randomLength = randomIntFromInterval(2, 4);
          var toTime = randomStartTime + randomLength;

          if(toTime > 17){
            toTime = 17;
            randomStartTime = 13;
          }

          var teachingHour = {
            speciality: specialitiesUser[i],
            startTime: randomStartTime,
            toTime: toTime,
            dayInWeek: randomIntFromInterval(0, 6)
          };

          teachingHours.push(teachingHour);
        }

        //store to mongodb
        var newUser = new User({
          firstName: user.name.first,
          lastName: user.name.last,
          displayName: user.name.first + ' ' + user.name.last,
          gender: user.gender,
          email: user.email,
          dob: new Date(user.dob * 1000),
          phone: user.phone,
          cell: user.cell,
          picture: user.picture,
          specialities: specialitiesUser,
          teachingHours: teachingHours
        });

        newUser.save(function(err) {
          if (err) {
            console.log('save error!');
          }

          callback();
        });

      }, function(err) {
        if (err) {
          console.log('A store failed to process');
        }

        cb();
      });
    } else {
      console.log('A request failed to process');

      cb();
    }
  });
}, function(err) {
  if (err) {
    console.log('error!');
  } else {
    console.log('processed successfully');
  }
});
